import time
import RPi.GPIO as G

from rhex import Leg, RHexControl, Timer

G.setmode(G.BCM)

def test0():
  import rhex
  rhex.main()

def test1():
  ixs = [0, 1, 2, 3, 4, 5]

  for ix in ixs:
    l = Leg(ix)
    l.setSpeed(-50)
    l.actuate()
    time.sleep(1)
    l.stop()

def test2():
  '''
    Leg synchro test
  '''
  legs = []
  ids = [1, 2, 5]

  for ix in ids:
    l = Leg(ix)
    l.initPosition()
    l.setState(l.WALK)
    legs.append(l)

  while 1:
    for l in legs:
      l.process()

def test3():
  '''
    Leg walking animation test
  '''
  l = Leg(2)
  l.initPosition()
  l.setState(l.WALK)

  while 1:
    l.process()

def test4():
  '''
    Leg rotation accuracy test
  '''
  l = Leg(1)
  l.initPosition()
  l.setSpeed(50)

  t = Timer(2)
  paused = False
  angle = 0
  while 1: 
    l.process()
    if paused:
      if t.isTime():
        angle = (angle + 90) % 360
        l.toRotation(angle)
        paused = False
        print("Turn to %i" % angle)
    else:
      if not l.isRunning():
        print("Ended at %i" % l.getAngle())
        paused = True
        t.setTime()

def test41():
  '''
    Leg 360deg rotation test
  '''
  l = Leg(1)
  while l.isRunning(): l.process()

  raw_input("Press enter to continue")
  l.initPosition()
  l.toRotation(0)
  l.setSpeed(50)
  while 1: l.process()

def test5():
  '''
    RHex startup movement test
  ''' 
  c = RHexControl() 

  c.process()
  while not c.isReady(): c.process()
  char = raw_input("Press enter to continue") 

  c.Move()
  while True:
    c.process()

def test6():
  '''
    RHex movement control test
  '''
  c = RHexControl() 

  c.process()
  while not c.isReady(): c.process()
  char = raw_input("Press enter to continue") 

  c.setState(c.WALK)
  c.resetLegPositions()
  while True:
    c.process()


if __name__=="__main__":
  test6()
