# -*- coding: utf-8 -*-
import time
import RPi.GPIO as G

from threading import Thread

from .utils import Timer
from .leg import *


class RHexControl():
  # Animation time constants
  INIT_DELAY = 1
  STAND_DELAY = 1.3

  # Direction constants
  FORWARD = 0
  REVERSE = 1
  UP = 2

  # State machine constants
  INIT = 0
  LYING = 1
  STAND = 2
  STANDING = 3
  WALK = 4
  WALKING = 5

  def __init__(self):
    self.legs = [None for i in range(len(leg_map))]
    self._initLegs()

    self.NEXT_STATE = None
    self.STATE_TIMER = Timer(0)
    self.setState(self.INIT)
    self.setSpeed(50)

    self.cmd_queue = []

  def _initLegs(self):
    i = 0;
    while(i < len(leg_map)):
      self.legs[i] = Leg(i)
      i = i + 1

  def standUp(self):
    self.setState(self.STAND)

  def getLeg(self, leg_id):
    return self.legs[leg_id]

  def setSpeed(self, value):
    self.speed = value

  def resetLegPositions(self):
    for l in self.legs:
      l.initPosition()

  def forward(self):
    for leg in self.legs:
      leg.setSpeed(50)

  def reverse(self):
    for leg in self.legs:
      leg.setSpeed(-50)

  def actuate(self):
    for leg in self.legs:
      leg.actuate()

  def stop(self):
    for leg in self.legs:
      leg.stop()

  def deactivate(self):
    for leg in self.legs:
      leg.deactivate()

  def setDirection(self, dir):
    if dir == self.FORWARD:
      print("FORWARD")
      self.cmd_queue.append(lambda: self.forward())
    elif dir == self.REVERSE:
      print("REVERSE")
      self.cmd_queue.append(lambda: self.reverse())

  def isStanding(self):
    return self.STATE == self.STANDING

  def isReady(self):
    for l in self.legs:
      if l.isRunning():
        return False
    return True

  def Move(self):
    self.setState(self.STAND)
    self.NEXT_STATE = self.WALK

  def _Move(self, flag=0):
    if flag == 0:
      if not self.isStanding():
        self.setState(self.STAND)
        self.cmd_queue.append(lambda: self.Move(flag=1))
        print("[Move]Waiting for stand position...")
    elif flag == 1:
      if not self.isStanding():
        self.cmd_queue.append(lambda: self.Move(flag=1))
        return
      else:
        print("[Move]Entering walk animation")
        self.setState(self.WALK)

  def Actuate(self):
    self.cmd_queue.append(lambda: self.actuate())

  def Stop(self):
    self.cmd_queue.append(lambda: self.stop())

  def setState(self, state):
    self.STATE = state
    self.PHASE = 0

  def nextState(self):
    if self.NEXT_STATE == None:
      #self.setState(self.LYING)
      return
    self.setState(self.NEXT_STATE)

  def process(self):
    cmd_queue = self.cmd_queue[:]
    for fn in cmd_queue:
      fn()
    self.cmd_queue = []

    st = self.STATE

    if st == self.INIT:
      done = True
      for l in self.legs:
        if l.isRunning():
          done = False
      if done:
        self.setState(self.LYING) 

    elif st == self.LYING:
      if self.PHASE == 0:
        self.STATE_TIMER.setPeriod(2)
        self.STATE_TIMER.setTime()
        self.PHASE = 1
      elif self.PHASE == 1 and self.STATE_TIMER.isTime():
        self.STATE_TIMER.setTime()

    elif st == self.STAND:
      if self.PHASE == 0:
        self.PHASE = 1
        self.forward()
        self.STATE_TIMER.setPeriod(self.STAND_DELAY)
        self.STATE_TIMER.setTime()
        self.actuate()
      elif self.PHASE == 1 and self.STATE_TIMER.isTime():
        self.stop()
        self.setState(self.STANDING)
        print("Standing")
    elif st == self.STANDING:
      self.nextState()

    elif st == self.WALK:
      if self.PHASE == 0:
        print("Assuming walk position")
        #for id in [0, 3, 4]:
        #  l = self.getLeg(id)
        #  l.toRotation(180)
        #  l.process()
        #  while l.isRunning(): l.process()
          
        self.PHASE = 1
      elif self.PHASE == 1:
        if self.isReady():
          print("Leg walk position init done")
          self.setState(self.WALKING)
    elif st == self.WALKING:
      if self.PHASE == 0:
        for l in self.legs:
          l.setSpeed(100)
        self.PHASE = 1
      elif self.PHASE == 1:
        if self.isReady(): self.PHASE = 2
      elif self.PHASE == 2:
        for id in [0, 3, 4]:
          l = self.getLeg(id)
          l.toRotation(360)
        self.PHASE = 3
      elif self.PHASE == 3:
        if self.isReady(): self.PHASE = 4
      elif self.PHASE == 4:
        for id in [1, 2, 5]:
          l = self.getLeg(id)
          l.toRotation(360)
        self.PHASE = 1
    else:
      self.setState(self.LYING)

    for leg in self.legs:
      leg.process()
