# -*- coding: utf-8 -*-
from rhex import main

from .leg import Leg
from .control import RHexControl
from .utils import Timer

if __name__=="__main__":
  main()
