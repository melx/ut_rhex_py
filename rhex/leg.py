# -*- coding: utf-8 -*-
import time, math
import RPi.GPIO as G

from .utils import Timer

# 0, 1, 2, 3, 4, 5
leg_map = [2, 3, 4, 17, 27, 22]

leg_ds_stop_map = [9.975, 10.805, 3.5535, 11.119, 11.9105, 4.76]
leg_ds_fw_map   = [4.975, 5.805, 0.1, 6.119, 16.9105, 0.1]
leg_ds_bk_map   = [14.975, 15.805, 8.5535, 16.119, 6.9105, 9.76]

v_map = {
  100: 5.70,    #5.58
  50: 3.58,     #3.58
  25: 4.22,     #1.76
  -100: -5.58,  #-5.58
  -50: -3.58,   #-3.58
  -25: -1.76,   #-1.76
  0: 0
  }


class Leg():
  # Leg animation timings
  INIT_DELAY = 1.5
  STAND_DELAY = 1

  # Leg position constants
  FR = 0
  FL = 1
  MR = 2
  ML = 3
  BR = 4
  BL = 5

  # Leg direction constants
  FORWARD = 0
  REVERSE = 1

  # Process states
  FREE = 0
  INIT = 1
  HOLD = 2
  WALK = 3
  TO_ROTATION = 4

  @staticmethod
  def getLeg(leg_id):
    return leg_map[leg_id]

  @staticmethod
  def getLegHoldDS(leg_id):
    return leg_ds_stop_map[leg_id]

  @staticmethod
  def getLegREVDS(leg_id):
    return leg_ds_bk_map[leg_id]

  @staticmethod
  def getLegFWDS(leg_id):
    return leg_ds_fw_map[leg_id]

  def __init__(self, id):
    self.id = id
    self.pin = Leg.getLeg(id)
    self.freq = 50
    self.dutycycle = 0
    self.running = False

    self.log("Init pin %i" % (self.pin))
    G.setup(self.pin, G.OUT)
    self.initPWM()

    self.DIRECTION = Leg.FORWARD
    self.STATE_TIMER = Timer(1)
    self.pos_proj_timer = Timer(1)
    self.setState(self.INIT)
    self.initPosition()
    self.setSpeed(-15)

  def initPWM(self):
    self.pwm = G.PWM(self.pin, self.freq)   
    if self.running:
      self.actuate()

  def setState(self, state):
    self.STATE = state
    self.PHASE = 0

  def setSpeed(self, percent):
    '''
      @param percent Range from -100 to 100
    '''
    #self.log("setSpeed %i" % percent)
    c = 1.5
    if percent < 0:
      #mx_ds = Leg.getLegREVDS(self.id)
      mx_ds = pow(-1, self.id) * c
    else:
      #mx_ds = Leg.getLegFWDS(self.id)
      mx_ds = pow(-1, self.id + 1) * c

    stop_ds = Leg.getLegHoldDS(self.id)

    ds = stop_ds + (mx_ds) * abs(percent) / 100.0
    self.setDuty(ds)
    self.speed = percent

  def log(self, msg):
    print("[Leg.id==%i]%s" % (self.id, msg))

  def setFreq(self, freq):
    self.freq = freq
    #self.log("Set freq=%f" % freq)
    self.pwm.ChangeFrequency(freq)

  def setDuty(self, ds):
    self.dutycycle = ds
    #self.log("Set dutycycle=%f" % ds)
    self.pwm.ChangeDutyCycle(ds)

  def actuate(self):
    self.setFreq(self.freq)
    self.pwm.start(self.dutycycle)
    self.setDuty(self.dutycycle)
    self.running = True
    self.log("Run")

  def isRunning(self):
    return self.running

  def stand(self):
    self.setDuty(Leg.getLegHoldDS(self.id))

  def forward(self):
    self.setDuty(Leg.getLegFWDS(self.id))

  def reverse(self):
    self.setDuty(Leg.getLegREVDS(self.id))

  def deactivate(self):
    self.pwm.stop()

  def stop(self):
    #self.stand()
    self.deactivate()
    #self.actuate()
    self.running = False
    self.log("Stop")

  def initPosition(self):
    self.ANGLE = 0
    self.v_project_time = time.time()

  def projectPosition(self):
    if not self.running:
      return
    if self.speed not in v_map:
      return

    v = v_map[self.speed]
    dt = time.time() - self.v_project_time

    a = self.ANGLE
    self.ANGLE = (self.ANGLE + v*dt  ) % (2*math.pi)
    self.v_project_time = time.time()

  def toRotation(self, value):
    self.TO_ANGLE = value % 360
    self.setState(self.TO_ROTATION)
    #self.log("toRotation(%i)" % value)

  def toDeltaRotation(self, value):
    self.TO_ANGLE = ( self.ANGLE + value ) % 360

  def getAngle(self):
    return self.ANGLE*float(180)/3.14

  def process(self):
    self.projectPosition()
    if self.pos_proj_timer.isTime():
      #self.log("ANGLE=%.2f" % (self.ANGLE*180/3.14))
      self.pos_proj_timer.setTime()

    st = self.STATE

    if st == self.INIT:
      if self.PHASE == 0:
        self.setSpeed(-10)
        self.PHASE = 1
        self.STATE_TIMER.setPeriod(self.INIT_DELAY)
        self.STATE_TIMER.setTime()
        self.actuate()
      elif self.STATE_TIMER.isTime():
          self.stop()
          self.initPosition()
          self.setState(self.FREE)

    elif st == self.FREE:
      if self.PHASE == 0:
        self.deactivate()
        self.PHASE = 1

    elif st == self.WALK:
      a = self.getAngle()
      if self.PHASE == 0:
        self.setSpeed(100)
        self.actuate()
        self.PHASE = 1
      elif self.PHASE == 1 and ( 0 < a < 250 or 290 < a < 360 ): 
        self.PHASE = 2
      elif self.PHASE == 2 and 260 < a < 280:
        self.projectPosition()
        #self.setSpeed(25)
        self.setState(self.HOLD)
        self.stand()

    elif st == self.HOLD:
      if self.PHASE == 0:
        self.stand()
        self.actuate()
        self.PHASE = 1

    elif st == self.TO_ROTATION:
      a = self.getAngle()
      b1 = (self.TO_ANGLE - 10) % 360
      b2 = (self.TO_ANGLE + 10 ) % 360

      #print(b1, b2, a)
      if b1 < b2: in_range = b1 < a < b2
      else: in_range = b1 < a <= 360 or 0 <= a < b2

      if self.PHASE == 0:
        self.actuate()
        self.last_angle = self.ANGLE

        if in_range: self.PHASE = 1
        else: self.PHASE = 2
      elif self.PHASE == 1:
        if not in_range:
          self.PHASE = 2
      elif self.PHASE == 2:
        #self.log("%i (target=%i)" (a, self.TO_ANGLE)) 
        if in_range:
          self.stop()
          self.setState(self.FREE)
          #self.log("Achieved rotation %i (%i)" % (self.ANGLE, a))
