#!/usr/bin/python
# -*- coding: utf-8 -*-
import RPi.GPIO as G

from threading import Thread

from .control import RHexControl

from .cmd import *
from .utils import Timer

ctrl_proc_active = True

def ctrl_proc_fn(c):
  while ctrl_proc_active:
    c.process()

def main():
  G.setmode(G.BCM)
  C = RHexControl()
  global C

  ctrl_proc = Thread( target=lambda: ctrl_proc_fn(C) )
  ctrl_proc.start()
  
  try:
    while 1:
      v = raw_input("<<< ").upper()
      if v.upper() == "EXIT":
        ctrl_proc_active = False
        raise KeyboardInterrupt()
      try:
        handle_input(v, C)
      except Exception, e:
        print(e)
  except KeyboardInterrupt: 
    pass
  finally:
    ctrl_proc_active = False
    ctrl_proc.join()
    G.cleanup()
    print("Cleanup done.")


if __name__=="__main__":
  main()
