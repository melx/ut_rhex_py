# -*- coding: utf-8 -*-

def handle_leg_cmd(leg_id, c):
  leg = c.getLeg(leg_id)
  cmd = ""
  while 1:
    cmd = raw_input("Command: ")
    if cmd.upper() == "END":
      break;
    try:
      getattr(leg, cmd)()
    except Exception, e:
      print(e)

def handle_input(v, c):
  if v == "F":
    c.setDirection(c.FORWARD)
  elif v == "R":
    c.setDirection(c.REVERSE)
  elif v == "A":
    c.actuate()
  elif v == "ST":
    c.standUp()
  elif v == "setStandDelay".upper():
    Control.STAND_DELAY = input("Stand delay: ")
  elif v == "LG":
    leg_id = input("Leg id? ")
    handle_leg_cmd(leg_id, c) 
  elif v == "FQ":
    leg_id = input("Leg id? ")
    freq = input("New frequency? ")
    c.getLeg(leg_id).setFreq(freq)
  elif v == "DS":
    leg_id = input("Leg id? ")
    ds = input("New dutycycle? ")
    c.getLeg(leg_id).setDuty(ds)
  elif v == "CMD":
    input()
  elif v == "D":
    c.deactivate()
  elif v == "S":
    c.stop()
  elif v == "M":
    print("c.Move()")
    c.Move()
  elif v == "END":
    G.cleanup()
  else:
    c.stop()

