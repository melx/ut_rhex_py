# -*- coding: utf-8 -*-
import time

class Timer():
  def __init__(self, period): 
    self.period = period
    self.started_at = time.time()
    self.fire_time = 0

  def isTime(self):
    if time.time() > self.fire_time:
      return True
    return False

  def setTime(self):
    self.started_at = time.time()
    self.fire_time = self.started_at + self.period

  def setPeriod(self, value):
    self.period = value
